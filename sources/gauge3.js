function Gauge(placeholderName, configuration) {
    this.placeholderName = placeholderName;

    var self = this; // pour les fonctions internes de d3

    this.configure = function (configuration) {
        this.config = configuration;

        this.config.size = this.config.size * 0.9;

        this.config.radius = this.config.size * 0.97 / 2;
        this.config.cx = this.config.size / 2;
        this.config.cy = this.config.size / 2;

        this.config.min = undefined != configuration.min ? configuration.min : 0;
        this.config.max = undefined != configuration.max ? configuration.max : 100;
        this.config.range = this.config.max - this.config.min;

        this.config.majorTicks = configuration.majorTicks || 5;
        this.config.minorTicks = configuration.minorTicks || 2;

        this.config.greenColor = configuration.greenColor || "#109618";
        this.config.yellowColor = configuration.yellowColor || "#FF9900";
        this.config.redColor = configuration.redColor || "#DC3912";

        this.config.transitionDuration = configuration.transitionDuration || 500;
    }

    this.render = function () {
        this.body = d3.select("#" + this.placeholderName)
            .append("svg:svg")
            .attr("class", "gauge")
            .attr("width", this.config.size)
            .attr("height", this.config.size);

        // Dessiner le corps de la jauge
        this.body.append("svg:circle")
            .attr("cx", this.config.cx)
            .attr("cy", this.config.cy)
            .attr("r", this.config.radius)
            .style("fill", "#ccc")
            .style("stroke", "#000")
            .style("stroke-width", "0.5px");

        // Dessiner les zones de couleur
        // (zones vertes, jaunes et rouges)
        // � ajouter ici selon la configuration

        // Dessiner les marqueurs sur la jauge
        // � ajouter ici selon la configuration

        // Initialiser le pointeur de la jauge
        // � ajouter ici

        // Mettre � jour la jauge avec la valeur initiale
        this.redraw(this.config.min, 0);
    }

    this.redraw = function (value, transitionDuration) {
        // Mettre � jour la position du pointeur pour refl�ter la nouvelle valeur
        // � impl�menter ici
    }

    // Initialisation
    this.configure(configuration);
    this.render();
}

// Fonction pour mettre � jour la jauge avec une nouvelle valeur de luminosit�
function updateGauge(value) {
    if (typeof gauge === 'undefined') {
        // Configuration de la jauge si elle n'est pas encore initialis�e
        var config = {
            size: 300,
            label: "Luminosit�",
            min: 0,
            max: 100,
            majorTicks: 5,
            greenZones: [{ from: 0, to: 30 }],
            yellowZones: [{ from: 30, to: 70 }],
            redZones: [{ from: 70, to: 100 }]
        };

        gauge = new Gauge("luminosityDisplay", config);
    }

    // Mettre � jour la jauge avec la nouvelle valeur de luminosit�
    gauge.redraw(value, 500); // La dur�e de transition est de 500 millisecondes
}
