document.addEventListener('DOMContentLoaded', function() {
  // Exemple de donn�es pour le diagramme de Kiviat
  var labels = ['Temperature', 'Vent', 'Pression', 'Eclairement'];
  var data = [20, 15, 1005, 300];

  var ctx = document.getElementById('weatherChart').getContext('2d');
  var weatherChart = new Chart(ctx, {
    type: 'radar',
    data: {
      labels: labels,
      datasets: [{
        label: 'Donnees Meteo',
        data: data,
        backgroundColor: 'rgba(75, 192, 192, 0.2)',
        borderColor: 'rgba(75, 192, 192, 1)',
        borderWidth: 2,
        pointRadius: 4,
      }]
    },
    options: {
      scales: {
        r: {
          suggestedMin: 0,
          suggestedMax: 40 // Ajustez cela en fonction de la plage de vos donn�es
        }
      }
    }
  });
});
