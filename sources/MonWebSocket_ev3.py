
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 20 10:47:42 2020

@author: Franck CANU
"""
'''importaton des différents modules nécessaires'''
import time
import json
from tornado import websocket, web, ioloop
from tornado.ioloop import PeriodicCallback
from ev3dev2.sensor import INPUT_1,INPUT_2,INPUT_3,INPUT_4 #on importe le module
from ev3dev.ev3 import Sensor#on importe le module

###############################################################

capteur_temp = Sensor(address=INPUT_1, driver_name='temp')
capteur_angle = Sensor(address=INPUT_2, driver_name='angle')
capteur_lum = Sensor(address=INPUT_3, driver_name='light')
capteur_pres = Sensor(address=INPUT_4, driver_name='barometric')


assert capteur_temp.connected, "Connecter un capteur température port 1"
assert capteur_angle.connected, "Connecter un capteur angle port 2"
assert capteur_lum.connected, "Connecter un capteur lum port 3"
assert capteur_pres.connected, "Connecter un capteur pres port 4"


#assertion si le capteur n'est pas connecté au port 1

date=0
#initialise date à 0
date0=time.monotonic()
#Génére un objet time
'''Les WebSockets permettent une communication bidirectionnelle entre le navigateur et le serveur.

'''
class MonWebSocket(websocket.WebSocketHandler):
  # Sous-classe ou objet pour créer un gestionnaire WebSocket de base.

  def open(self):
    print ('Connection etablie avec le socket.')
#Méthode appelée lorsque le WebSocket est ouvert pour lancer l'envoi des données toutes les secondes
    self.callback = PeriodicCallback(self.envoie_donnees, 100)
    self.callback.start()

  def on_close(self):
    print ('Connection avec le socket ferme.')
    self.callback.stop()
#Méthode appelée lorsque le WebSocket est fermé.
#Si la connexion a été correctement fermée et qu'un code d'état ou une phrase de motif a été fournie

  def check_origin(self, origin):
    return True
#Méthode pour activer la prise en charge et autoriser d'autres origines.
#L'argument origin est la valeur de l'en-tête HTTP Origin, l'url responsable du lancement de cette demande.
#Remplacez pour activer la prise en charge pour autoriser d'autres origines.
#Doit retourner True pour accepter la demande ou False pour la rejeter.
#Il s'agit d'une protection de sécurité contre les attaques de script sur les navigateurs

  def envoie_donnees(self):
    print ("Les donnees sont envoyees")
 # Fonction pour envoyer les données
    date = round(time.monotonic() - date0,1);
    temp = (capteur_temp.value()*10)/100;
    angle = capteur_angle.value();
    lum = capteur_lum.value()/10; # à voir
    pres = round(capteur_pres.value()*4,1)# av

#génére une date en seconde
#Créér un dictionnaire contenant les données précédentes
    donnees = {

	   'x': date,
       'temp':temp,
       'angle':angle,
       'lum': lum,
       'pres': pres

    }

    print (donnees)

    #Ecrit un object json contenant les données dans le websocket dumps sérialise vers un JSON formaté str
    self.write_message(json.dumps(donnees))




if __name__ == "__main__":
  #créer une nouvelle application web avec un point de terminaison websocket disponible sur websocket

  print ("Demarrage du programme du serveur websocket. En attente des demandes des clients pour ouvrir le websocket ...")
  application = web.Application([(r'/', MonWebSocket),])
#Démarre un serveur pour cette application sur le port donné en gérant les répertoires courants.
#Il s'agit d'un alias pratique pour créer un objet HTTPServer et appeler sa méthode Listen.
  application.listen(8002)
#Les données (l'écoute) sont fournies sur le port TCP 8001
  ioloop.IOLoop.current().start()
#On démare la boucle d'échange d'E/S dans le socket créé












