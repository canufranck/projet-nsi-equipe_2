const express = require('express');
const app = express();
const port = 3000;

app.use(express.json());

app.post('/getEv3Data', (req, res) => {
  // Remplacez ceci par la logique pour obtenir les donn�es depuis l'EV3
  const ev3Data = {
    temperature: 25,
    wind: 10,
    pressure: 1010,
    brightness: 300
  };

  res.json(ev3Data);
});

app.listen(port, () => {
  console.log(`Server listening at http://localhost:${port}`);
});
